SOURCES=$(shell rename 's/ /_/g' *.fdb; find * -iname "*.fdb" -not -iname "*_quality.fdb" -not -iname "insert-borehole-grades.fdb" -type f -print)
TARGETS=$(SOURCES:.fdb=_quality.fdb)

.PHONY: default
#default: deploy-borehole-grades
default: $(TARGETS)

list-targets:
	@echo $(SOURCES)
	@echo $(TARGETS)

%_quality.fdb: %.fdb borehole.csv insert-borehole-grades.fdb
	@echo "Inserting data into Firebird database"
	mkdir -p ~/d/jupiter/quality/
	mv $< ~/d/jupiter/quality/
	# insert grades and ignore errors
	-~/c/Program\ Files/Firebird/Firebird_2_5/bin/isql.exe -user sysdba -password masterkey -i insert-borehole-grades.fdb \
		"$(hostname):D:\jupiter\quality\$<"
	# it can take a while before changes are propagated to the file
	sleep 5m
	# add _quality postfix to filename to note that it is done
	mv ~/d/jupiter/quality/$< $@

insert-borehole-grades.fdb: borehole.csv
	@echo "Preparing data insert for Firebird database"
	echo "alter table BOREHOLE add QUALITY_LOCATION int;" > $@
	echo "alter table BOREHOLE add QUALITY_DATA int;" >> $@
	echo "alter table BOREHOLE add QUALITY_SUM int;" >> $@
	cat $< | \
		awk -F ";" \
		'{ print "update BOREHOLE set QUALITY_LOCATION = " $$2 ", QUALITY_DATA = " $$3 ", QUALITY_SUM = " $$4 " where BOREHOLENO = " "\x27" $$1 "\x27" ";" }' >> $@

borehole.csv:
	@echo "Fetching quality data from MST-GKO PostgreSQL database"
	PGPASSWORD=gebr470ne psql -h 10.33.131.50 -p 5432 \
			   -d pcjupiterxl -U grukosreader -c \
			   "COPY jupiter.borehole (boreholeno, quality_location, quality_data, quality_sum) TO STDOUT DELIMITER ';' CSV;" \
			   > "$@"

.PHONY: clean
clean:
	$(RM) borehole.csv
	$(RM) insert-borehole-grades.fdb
